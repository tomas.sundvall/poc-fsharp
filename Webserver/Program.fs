open Suave
open Suave.Filters
open Suave.Operators
open Suave.Successful

// [<EntryPoint>]
let app =
  choose
    [ GET >=> choose
        [ path "/hello" >=> OK "GET resource /hello" 
          path "goodbye" >=> OK "GET resource /goodbye" ]
      POST >=> choose
        [ path "/hello" >=> OK "POST resource /hello" 
          path "goodbye" >=> OK "POST resource /goodbye"] ]

startWebServer defaultConfig app

          
// open Suave
// open Suave.Filters
// open Suave.Operators
// open Suave.Successful

// let app =
//   choose
//     [ GET >=> choose
//         [ path "/hello" >=> OK "Hello GET"
//           path "/goodbye" >=> OK "Good bye GET" ]
//       POST >=> choose
//         [ path "/hello" >=> OK "Hello POST"
//           path "/goodbye" >=> OK "Good bye POST" ] ]

// startWebServer defaultConfig app
// let main argv = 
//   let cts = new CancellationTokenSource()
//   let conf = { defaultConfig with cancellationToken = cts.Token }
//   let listening, server = startWebServerAsync conf (Successful.OK "Hello World")
    
//   Async.Start(server, cts.Token)
//   printfn "Make requests now"
//   Console.ReadKey true |> ignore
    
//   cts.Cancel()

//   0 // return an integer exit code